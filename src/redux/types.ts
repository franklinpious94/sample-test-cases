export const INCREMENT_ONE = "INCREMENT_ONE";
export const INCREMENT_BY_COUNT = "INCREMENT_BY_COUNT";

export type IncrementByOneAction = {
    type: typeof INCREMENT_ONE
}

export type IncrementByCountAction = {
    type: typeof INCREMENT_BY_COUNT,
    count: number
}