import React from "react";
import { render } from "../../test/test-utils";
import LayoutContainer from "./LayoutContainer";
import {
  createHistory,
  createMemorySource,
  LocationProvider,
} from "@reach/router";
import "@testing-library/jest-dom/extend-expect";

function renderWithRouter(
  ui,
  { route = "/", history = createHistory(createMemorySource(route)) } = {}
) {
  return {
    ...render(<LocationProvider history={history}>{ui}</LocationProvider>),
    history,
  };
}

test("landing on a bad page", () => {
  const { container } = renderWithRouter(<LayoutContainer />, {
    route: "/non-matching-route",
  });
  expect(container.textContent).toMatch("Not Found");
});

test("landing on about page", () => {
  const { container } = renderWithRouter(<LayoutContainer />, {
    route: "/about",
  });
  expect(container.textContent).toMatch("About");
});

test("landing on home page", () => {
  const { container } = renderWithRouter(<LayoutContainer />, {
    route: "/",
  });
  expect(container.textContent).toMatch("Welcome!");
});
