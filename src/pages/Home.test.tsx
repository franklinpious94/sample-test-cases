import React from 'react';
import { render } from '../../test/test-utils';
import Home from './Home';
import '@testing-library/jest-dom/extend-expect';

it('renders <Home /> page', () => {
  // You should be able to show that you can verify Home rendered correctly
  render(<Home />)

  const headerElement = document.querySelector("h1[data-testid='page-header']");
  expect(headerElement?.textContent).toBe("Welcome!");

  const aboutPageLinkElement = document.querySelector("a[data-testid='about-page-link");
  expect(aboutPageLinkElement?.textContent).toBe("Go to about");
  expect(aboutPageLinkElement).toHaveAttribute('href', '/about')

});
