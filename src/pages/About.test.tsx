import React from "react";
import userEvent from "@testing-library/user-event";
import { render } from "../../test/test-utils";
import { within } from "@testing-library/dom";
import About from "./About";
import {
  screen
} from "@testing-library/react";
import { incrementOne } from "../redux/actions/sample";
import * as redux from "react-redux";

it("renders <About /> page", () => {
  // You should be able to verify the About page rendered correctly.
  render(<About />);
  const sectionElement = document.querySelector("section") as HTMLElement;
  const pageheader = within(sectionElement).queryByTestId("page-header");
  expect(pageheader?.textContent).toBe("About Page");
});

it("clicks button and fires increment counter", () => {
  render(<About />);

  const buttonElement = screen.queryByTestId("increment-button") as HTMLElement;

  expect(buttonElement.textContent).toBe("Increment");
  const useDispatchMock = jest.spyOn(redux, "useDispatch");
  const dummyDispatch = jest.fn();
  useDispatchMock.mockReturnValue(dummyDispatch(incrementOne));
  userEvent.click(buttonElement);
  expect(dummyDispatch).toHaveBeenCalledWith(incrementOne);
});

it("displays current count", () => {
  const state = {
    sample: {
      counter: 4
    }
  }
  const useSelectorMock = jest.spyOn(redux, "useSelector");
  useSelectorMock.mockReturnValue(state.sample.counter);
  render(<About />);
  const countElement = screen.queryByTestId("count-value") as HTMLElement;
  expect(countElement.textContent).toBe("Current Count: 4");
});
